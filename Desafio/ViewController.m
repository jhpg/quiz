//
//  ViewController.m
//  Desafio
//
//  Created by Jorge Henrique P. Garcia on 23/02/15.
//  Copyright (c) 2015 Jorge Henrique P. Garcia. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end


@implementation ViewController

NSMutableArray *perguntas, *respostas, *imagens;
int numPergunta = 0;
bool trava = false;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    perguntas = [NSMutableArray arrayWithObjects: @"Em que ano estamos?", @"Em que SO está rodando esse app?", @"Qual a última versão do OS X?", nil];
    respostas = [NSMutableArray arrayWithObjects: @"2015", @"iOS", @"Yosemite 10.10", nil];
    
    imagens = [NSMutableArray arrayWithObjects: @"2015.jpg", @"ios8.jpg", @"Yosemite.jpg", nil];
    
    [_lbPergunta setText:@"???"];
    [_lbResposta setText:@"???"];
    
    _btPergunta.backgroundColor = [UIColor colorWithRed:255 green:100 blue:0 alpha:0.7];
    _btResposta.backgroundColor = [UIColor colorWithRed:255 green:100 blue:0 alpha:0.7];
    _lbPergunta.textColor = [UIColor whiteColor];
    _lbResposta.textColor = [UIColor whiteColor];
    
    
    /* Parte do exercício
     
    self.view.backgroundColor = [UIColor blueColor];    //Altera fundo da view
    
    _btPergunta.backgroundColor = [UIColor orangeColor];
    _btResposta.backgroundColor = [UIColor orangeColor];
    //_btResposta.backgroundColor = [UIColor colorWithRed:250 green:128 blue:114 alpha:1];
    
    _btPergunta.tintColor = [UIColor whiteColor];
    _btResposta.tintColor = [UIColor whiteColor];
    
    _lbPergunta.textColor = [UIColor whiteColor];
    _lbResposta.textColor = [UIColor whiteColor];
    
     */
    
    //self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"2015.jpg"]];  //Mudar
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        [self mostrarPergunta];
    }
}


- (IBAction)btPergunta:(id)sender{
    
    [self mostrarPergunta];
}

- (void) mostrarPergunta {
    
    [_lbResposta setText:@"???"];
    [_image setImage: nil];
    
    [_lbPergunta setText: [perguntas objectAtIndex: numPergunta]];
    
    trava = false;
}

- (IBAction)btResposta:(id)sender {
    
    if (trava == false){

        [_lbResposta setText: [respostas objectAtIndex: numPergunta]];
                               
        UIImage *imagem = [UIImage imageNamed: [imagens objectAtIndex: numPergunta]];

        _image.layer.cornerRadius = 10;
        _image.layer.masksToBounds = YES;


        [_image setImage: imagem];
        [_image sizeToFit];

        if(numPergunta < 2)
            numPergunta++;
        else
            numPergunta = 0;
            
        trava = true;
    }
    
}
@end
