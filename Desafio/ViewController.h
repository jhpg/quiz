//
//  ViewController.h
//  Desafio
//
//  Created by Jorge Henrique P. Garcia on 23/02/15.
//  Copyright (c) 2015 Jorge Henrique P. Garcia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lbPergunta;
@property (weak, nonatomic) IBOutlet UILabel *lbResposta;

@property (weak, nonatomic) IBOutlet UIButton *btPergunta;
@property (weak, nonatomic) IBOutlet UIButton *btResposta;

@property (weak, nonatomic) IBOutlet UIImageView *image;


- (IBAction)btPergunta:(id)sender;
- (IBAction)btResposta:(id)sender;


@end
