//
//  AppDelegate.h
//  Desafio
//
//  Created by Jorge Henrique P. Garcia on 23/02/15.
//  Copyright (c) 2015 Jorge Henrique P. Garcia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

